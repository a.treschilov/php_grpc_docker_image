FROM php:8.3.11-fpm

RUN apt-get update && apt-get install g++ -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
        libzip-dev \
        libicu-dev \
        nano \
        g++

RUN docker-php-ext-install -j$(nproc) iconv
RUN docker-php-ext-install mysqli pdo pdo_mysql
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install zip
RUN docker-php-ext-install sockets
RUN docker-php-ext-install intl
RUN docker-php-ext-enable intl

RUN pecl install grpc \
    && docker-php-ext-enable grpc

# Install composer
RUN apt-get update
RUN apt-get install -y \
    git

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

RUN cd /tmp \
    && git clone -b v1.66.1 --depth 1 https://github.com/grpc/grpc \
    && cd grpc